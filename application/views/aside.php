<aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="assets/img/avatar3.png" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>
                                <?php 
                                if ($this->session->userdata('username') == 'admin') {
                                    ?>
                                    <a href="app/profiladmin/<?php echo $this->session->userdata('id_user'); ?>">
                                 <?php echo $this->session->userdata('username'); ?></a>
                                    <?php
                                } else {

                                 ?>
                                <a href="app/profiluser/<?php echo $this->session->userdata('id_user'); ?>">
                                 <?php echo $this->session->userdata('username'); ?></a>
                                <?php } ?>
                            </p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>

                    <ul class="sidebar-menu">
                        <?php
                        if ($this->session->userdata('level') == 'admin') {
                         ?>
                        <li>
                            <a href="pelanggan">
                                <i class="fa fa-dashboard"></i> <span>Data pelanggan</span>
                            </a>
                        </li>

                        <?php } ?>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>