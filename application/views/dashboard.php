<div class="row">
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-success">
            <div class="inner">
                <h3><?= count($category); ?></h3>

                <p>Category</p>
            </div>
            <div class="icon">
                <i class="fa fa-newspaper"></i>
            </div>
            <a href="<?= base_url('category'); ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-info">
            <div class="inner">
                <h3><?= count($posts); ?></h3>

                <p>Posts</p>
            </div>
            <div class="icon">
                <i class="fa fa-clone"></i>
            </div>
            <a href="<?= base_url('post/data'); ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-warning">
            <div class="inner">
                <h3><?= count($members); ?></h3>

                <p>Members</p>
            </div>
            <div class="icon">
                <i class="ion ion-person-add"></i>
            </div>
            <a href="<?= base_url('member'); ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-danger">
            <div class="inner">
                <h3><?= count($comments); ?></h3>

                <p>Comments</p>
            </div>
            <div class="icon">
                <i class="fa fa-images"></i>
            </div>
            <a href="<?= base_url('comment'); ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
        </div>
    </div>

<div class="col-md-9">
    <h1 class="big-title">
        SIMPANLAH FILEMU SEBELUM TERHAPUS
    </h1>
    <p>Reposign adalah website yang berisi tentang repositori file design. Pada website ini
        kita dapat menyimpan, mengupload, melihat, dan mendownload file design. Selain itu,
        kita dapat berdiskusi dan memberi komentar pada setiap file design yang tersimpan.
    </p>
    <a class="btn btn-secondary" type="button" data-dismiss="modal" href="dashboard">Read more</a>
    </div>
    <div class="brand-image ">
        <img src="<?= base_url(); ?>\assets\dist\img\homee.png" alt="home">
    </div>
</div>

